﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using HuffmanFileWork;

namespace Huffman
{
    class HuffmanTree
    {
        public char ch { get; private set; }
        public double freq { get; private set; }
        public bool isTerminal { get; private set; }
        public HuffmanTree left { get; private set; }
        public HuffmanTree rigth { get; private set; }
        public HuffmanTree(char c, double frequency)
        {
            ch = c;
            freq = frequency;
            isTerminal = true;
            left = rigth = null;
        }
        public HuffmanTree(HuffmanTree l, HuffmanTree r)
        {
            freq = l.freq + r.freq;
            isTerminal = false;
            left = l;
            rigth = r;
        }
    }
    class HuffmanInfo
    {
        HuffmanTree Tree;

        Dictionary<char, string> Codes = new Dictionary<char, string>();

        Dictionary<string, char> Chars = new Dictionary<string, char>();

        public HuffmanInfo(string fileName)
        {
            var huffmanNodes = new List<HuffmanTree>();

            using (StreamReader sr = new StreamReader(fileName, Encoding.Unicode))
            {
                var newLineFrequency = double.Parse(sr.ReadLine());
                huffmanNodes.Add(new HuffmanTree('\n', newLineFrequency));

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var ch = line[0];
                    var frequency = double.Parse(line.Substring(2));

                    huffmanNodes.Add(new HuffmanTree(ch, frequency));
                }
            }

            while (huffmanNodes.Count > 1)
            {
                huffmanNodes = huffmanNodes.OrderBy(node => node.freq).ToList();
                var newNode = new HuffmanTree(huffmanNodes[0], huffmanNodes[1]);
                huffmanNodes.RemoveRange(0, 2);
                huffmanNodes.Add(newNode);
            }

            Tree = huffmanNodes[0];

            MakeTables(Tree, "");
        }
        
        private void MakeTables(HuffmanTree currentNode, string code)
        {
            if (currentNode.isTerminal)
            {
                Codes[currentNode.ch] = code;
                Chars[code] = currentNode.ch;
            }
            else
            {
                MakeTables(currentNode.left,  code + "1");
                MakeTables(currentNode.rigth, code + "0");
            }
        }

        public void Compress(string inpFile, string outFile)
        {
            var sr = new StreamReader(inpFile, Encoding.Unicode);
            var sw = new ArchWriter(outFile);
            string line;
            while ((line = sr.ReadLine()) != null)
            {
                var encryptedLine = line.Select(ch => Codes[ch]);
                sw.WriteWord(string.Join("", encryptedLine));
                sw.WriteWord(Codes['\n']);
            }
            sr.Close();
            sw.Finish();
        }

        public void Decompress(string archFile, string txtFile)
        {
            var sr = new ArchReader(archFile);
            var sw = new StreamWriter(txtFile, false, Encoding.Unicode);
            byte curBit;
            string currentCode = "";
            while (sr.ReadBit(out curBit))
            {
                currentCode += curBit == 0 ? '0' : '1';

                if (Chars.ContainsKey(currentCode))
                {
                    sw.Write(Chars[currentCode]);
                    currentCode = string.Empty;
                }
            }
            sr.Finish();
            sw.Close();
        }
    }

    class Huffman
    {
        static void Main(string[] args)
        {
            if (!File.Exists("freq.txt"))
            {
                Console.WriteLine("Не найден файл с частотами символов!");
                return;
            }
            if (args.Length == 0)
            {
                var hi = new HuffmanInfo("freq.txt");
                hi.Compress("etalon.txt", "etalon.arc");
                hi.Decompress("etalon.arc", "etalon_dec.txt");
                return;
            }
            if (args.Length != 3 || args[0] != "zip" && args[0] != "unzip")
            {
                Console.WriteLine("Синтаксис:");
                Console.WriteLine("Huffman.exe zip <имя исходного файла> <имя файла для архива>");
                Console.WriteLine("либо");
                Console.WriteLine("Huffman.exe unzip <имя файла с архивом> <имя файла для текста>");
                Console.WriteLine("Пример:");
                Console.WriteLine("Huffman.exe zip text.txt text.arc");
                return;
            }
            var HI = new HuffmanInfo("freq.txt");
            if (args[0] == "zip")
            {
                if (!File.Exists(args[1]))
                {
                    Console.WriteLine("Не найден файл с исходным текстом!");
                    return;
                }
                HI.Compress(args[1], args[2]);
            }
            else
            {
                if (!File.Exists(args[1]))
                {
                    Console.WriteLine("Не найден файл с архивом!");
                    return;
                }
                HI.Decompress(args[1], args[2]);
            }
            Console.WriteLine("Операция успешно завершена!");
        }
    }
}
